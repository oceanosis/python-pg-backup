.PHONY: default install test

default: test

install:
	PIPENV_IGNORE_VIRTUALENVS=1
	pipenv install --dev --skip-lock

test:
	PYTHONPATH=./src pytest

