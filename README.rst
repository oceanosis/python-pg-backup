python-pgbackup
========

CLI for backing up remote PostgreSQL databases locally or to AWS S3.

Preparing for Development
-------------------------

# Virtual host for DB & Containers

1. Ensure Vagrant and VBox are installed [ example:oceanosis/centos7-ansible-awscli ]
2. Create Centos7 host with Vagrantfile with defined public (bridged) network
3. ``vagrant ssh`` to centos, install docker and run postgres:9.6.8-alpine container with user and pass you defined, expose port 5432 as you wish.

# Local / Desktop Conf.

4. Ensure ``pip`` and ``venv`` are installed
5. Clone repository: ``git clone git@bitbucket.org:oceanosis/python-pg-backup.git``
6. ``cd`` into repository 
7. Fetch development dependencies ``make install``
8. Activate virtualenv: ``source activate``

Usage
-----

Pass in a full database URL, the storage driver, and destination.

S3 Example w/ bucket name:

::

    $ pgbackup postgres://test@postgresql.host.ip:5432/db_one --driver s3 backups

Local Example w/ local path:

::

    $ pgbackup postgres://test@postgresql.host.ip:5432/db_one --driver local /var/local/db_one/backups

Running Tests
-------------

Run tests locally using ``make`` if virtualenv is active:

::

    $ make

